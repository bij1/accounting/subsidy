# BIJ1 subsidy

Hier onderzoeken we het aantal leden op basis waarvan de partij voor subsidie
in aanmerking zou komen o.b.v. de
[Wet financiering politieke partijen](https://wetten.overheid.nl/BWBR0033004/2022-05-01).

## gebruikte tools

- Docker: helpt om software te runnen
- Jupyter: coden in je browser
- SQLite3: simpele database

## requirements

- [Docker](https://www.docker.com/)
- Mollie CSVs put in this directory: `payments.csv` + `refunds.csv` + `chargebacks.csv`

## usage

- run in the console:
```bash
docker run -p 8888:8888 -v $(pwd):/home/jovyan/work -e GRANT_SUDO=yes --user root jupyter/scipy-notebook
```
- open the resulting URL in your browser
- navigate to and open `bij1-subsidy.ipynb`
- run all cells

